using UnityEngine;
using UnityEngine.Events;

namespace BenlaodAssets.GameEvent.Runtime.Scripts.Events
{
    public class GameEventListener : MonoBehaviour
    {
        public GameEvent @event;
        public UnityEvent<EventData> response;

        private void OnEnable()
        {
            @event.RegisterListener(this);
        }

        private void OnDisable()
        {
            @event.UnregisteredListener(this);
        }

        public void OnEventRaised(EventData value)
        {
            response.Invoke(value);
        }
    }
}