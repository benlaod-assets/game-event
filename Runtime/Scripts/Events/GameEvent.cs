using System.Collections.Generic;
using UnityEngine;

namespace BenlaodAssets.GameEvent.Runtime.Scripts.Events
{
    [CreateAssetMenu(fileName = "GameEvent", menuName = "Tools/Events/Game Event", order = 0)]
    public class GameEvent : ScriptableObject
    {
        private readonly List<GameEventListener> _listeners = new();

        public void Raise(object value = null)
        {
            for (var i = _listeners.Count - 1; i >= 0; i--) _listeners[i].OnEventRaised(value != null ? new EventData(value) : new EventData());
        }

        public void RegisterListener(GameEventListener listener)
        {
            _listeners.Add(listener);
        }

        public void UnregisteredListener(GameEventListener listener)
        {
            _listeners.Remove(listener);
        }
    }
}