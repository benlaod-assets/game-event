﻿namespace BenlaodAssets.GameEvent.Runtime.Scripts
{
    public class EventData
    {
        private readonly object _data;

        public EventData(object data = null)
        {
            _data = data;
        }

        public T GetData<T>()
        {
            return _data != null ? (T)_data : default;
        }
    }
}