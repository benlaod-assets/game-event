## Required in
- [Dialogue System](https://gitlab.com/benlaod-assets/dialogue-system)

## How to install it ?

In your package manager, click on the '+' button then 'Add package by name...'.
Then past the following URL `fr.benjaminbrasseur.gameevent`.
